# Coding theory
## Syndrome decoding using standard (Slepian) array

### Requirements:
* Python version: 3.5+

### Input/Output format
##### Linear code:
* Contains multiple lines, elements are separated by spaces
    1. `Q (alphabet)`  
    E.g. `0 1 2` (ternary code)
    2. `k (message length)` `n (block length)`  
    E.g. `3 6`
    3. `C` or `G` or `H` or `S` or their combination, which tells what other data is specified in the following rows.  
        `C` - codewords (each codeword in a separate row) | E.g. `000 000000` if the original message is `000` and the codeword for that is `000000`  
        `G` - generator matrix | E.g.  
            `011110`  
            `100110`  
            `001111`  
        `H` - parity-check matrix | same format as the generator matrix's  
        `S` - standard (Slepian) array | E.g.  
            `000 00000 01011 10101 11110`  
            `101 00001 01010 10100 11111`  
            `...`  
          
        E.g.`C G` means that the next rows contains the codewords followed by the generator matrix  
    4. **Important:** Leave an empty line before each blocks  
        E.g. the structure of `C G` is: *empty_line*, *codewords*, *empty_line*, *generator_matrix*  

* Example input files (for binary codes) are located in the `inputs/binary_codes/examples` folder

##### Binary code:
*  The same as the **Linear code**, except that the first row (`alphabet`) must be omitted.
