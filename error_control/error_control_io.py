from typing import Optional, Union, Dict, List

from error_control.linear_code import LinearCode, BinaryCode


class ECIO:
    """ Error Control Input/Output methods """
    @staticmethod
    def read_lin_code_from_file(filename: str, is_binary: Optional[bool] = True) -> Union[LinearCode, BinaryCode]:
        """ Reads and returns a linear code from the specified file """
        with open(filename) as file:
            lines = file.read().splitlines()

        if is_binary:
            min_lines = 4
        else:
            min_lines = 5

        if len(lines) < min_lines:
            raise IOError("The input file must contain at least " + str(min_lines) + " lines")

        format_error = IOError("Wrong input file format!")

        alphabet = None
        if not is_binary:
            alphabet = lines.pop(0).split()
            if len(alphabet) < 1:
                raise format_error

        elems = lines.pop(0).split()
        if len(elems) != 2:
            raise format_error
        message_length = int(elems[0])
        block_length = int(elems[1])

        if is_binary:
            linear_code = BinaryCode(message_length, block_length)
        else:
            linear_code = LinearCode(alphabet, message_length, block_length)

        available_blocks = ('C', 'G', 'H', 'S')

        blocks = lines.pop(0).split()
        for block in blocks:
            if not (block in available_blocks):
                raise format_error

            lines.pop(0)
            block_lines = []

            while len(lines) > 0 and len(lines[0].strip()) > 0:
                block_lines.append(lines.pop(0).strip())

            if block == 'C':
                linear_code.set_codewords(ECIO.__build_codewords(block_lines))
            elif block == 'G':
                linear_code.set_generator_matrix(block_lines)
            elif block == 'H':
                linear_code.set_parity_check_matrix(block_lines)
            else:   # block == 'S'
                linear_code.set_standard_array(ECIO.__build_standard_array(block_lines))

        return linear_code

    @staticmethod
    def __build_codewords(block_lines: List[str]) -> Dict[str, List[str]]:
        """ Parses the input lines and creates the dictionary of codewords """
        codewords = {}
        for line in block_lines:
            elems = line.split()
            if len(elems) != 2:     # message codeword
                raise IOError("Wrong input file format!")
            codewords[elems[0]] = list(elems[1])
        return codewords

    @staticmethod
    def __build_standard_array(block_lines: List[str]) -> List[List[str]]:
        """ Parses the input lines and creates the Slepian (two dimensional) array """
        standard_array = []
        if len(block_lines) > 0:
            nr_of_elems = len(block_lines[0].split())
            for line in block_lines:
                elems = line.split()
                if len(elems) != nr_of_elems:   # the number of columns must be consistent
                    raise IOError("Wrong input file format!")

                standard_array.append(elems)
        return standard_array

    @staticmethod
    def write_lin_code(linear_code: Union[LinearCode, BinaryCode]) -> None:
        print("Q = " + str(linear_code.get_alphabet()))
        print("k = " + str(linear_code.get_message_length()))
        print("n = " + str(linear_code.get_block_length()))

        print("C = [")
        for elems in linear_code.get_codewords().items():
            print("\t" + elems[0] + " -> " + "".join(elems[1]))
        print("]")

        print("G = [")
        for row in linear_code.get_generator_matrix():
            print("\t" + "".join(row))
        print("]")

        print("H = [")
        for row in linear_code.get_parity_check_matrix():
            print("\t" + "".join(row))
        print("]")

        print("S = [")
        for row in linear_code.get_standard_array():
            print("\t", end='')
            for column in row:
                print(''.join(column), end=' ')
            print()
        print("]")
