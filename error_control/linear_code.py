from typing import List, Union, Dict, Tuple

import sys


class LinearCode:
    """ Represents a linear code """
    _alphabet = None                # type: List[str]               # Q (symbol alphabet)
    _message_length = None          # type: int                     # k (dimension, original message words' length)
    _block_length = None            # type: int                     # n (codewords' length)
    _min_distance = None            # type: int                     # minimum Hamming distance
    _min_weight = None              # type: int                     # minimum Hamming weight
    _codewords = None               # type: Dict[str, List[str]]    # C
    _generator_matrix = None        # type: List[List[str]]         # G
    _parity_check_matrix = None     # type: List[List[str]]         # H
    _standard_array = None          # type: List[List[str]]         # Slepian array

    def __init__(self, alphabet: Union[list, tuple], message_length: int, block_length: int) -> None:
        """ Constructor """
        if message_length >= block_length:
            raise LinearCodeError("The blocks (codewords) should be longer than the original messages")
        self.set_alphabet(alphabet)
        self.set_message_length(message_length)
        self.set_block_length(block_length)

    def get_alphabet(self) -> List[str]:
        """ Returns the alphabet """
        if self._alphabet is None:
            raise LinearCodeError("The alphabet has not been set")
        return self._alphabet

    def set_alphabet(self, alphabet: Union[List[str], Tuple[str]]) -> None:
        """ Sets the alphabet if it's format is accepted """
        if type(alphabet) is not list and type(alphabet) is not tuple:
            raise LinearCodeError("The alphabet must be given a list or tuple")

        self._alphabet = []
        for symbol in alphabet:
            if not isinstance(symbol, str) or len(symbol) != 1:
                raise LinearCodeError("The symbols of the alphabet must be 1 letter strings")
            self._alphabet.append(symbol)

    def get_message_length(self) -> int:
        """ Returns k (original words' length) """
        if self._message_length is None:
            raise LinearCodeError("The message length has not been set")
        return self._message_length

    def set_message_length(self, message_length: int) -> None:
        """ Sets the k (original words' length) """
        if message_length < 1:
            raise LinearCodeError("The messages must contain at least 1 symbol")
        self._message_length = message_length

    def get_block_length(self) -> int:
        """ Returns n (codewords' length) """
        if self._block_length is None:
            raise LinearCodeError("The block length has not been set")
        return self._block_length

    def set_block_length(self, block_length: int) -> None:
        """ Sets the n (codewords' length) """
        if block_length < 1 or (self._message_length is not None and self._message_length > block_length):
            raise LinearCodeError("Codewords must be at least as long as the messages (and at least 1 symbol long)")
        self._block_length = block_length

    @staticmethod
    def get_distance(codeword1: str, codeword2: str) -> int:
        """ Returns the Hamming distance between two codewords """
        if len(codeword1) != len(codeword2):
            raise LinearCodeError("codeword1 and codeword2 must have the same length for distance calculation")

        distance = 0
        for i in range(len(codeword1)):
            if codeword1[i] != codeword2[i]:
                distance += 1
        return distance

    def get_min_distance(self) -> int:
        """ Returns the minimum Hamming distance of the codewords """
        if self._min_distance is None:
            # Could be improved to use the get_min_weight()
            self._min_distance = sys.maxsize
            codewords = self.get_codewords()
            for i in range(len(codewords)):     # compare each codeword pairs
                for j in range(i + 1, len(codewords)):
                    distance = LinearCode.get_distance(codewords[i], codewords[j])
                    if distance < self._min_distance:
                        self._min_distance = distance

        return self._min_distance

    @staticmethod
    def get_weight(codeword: str) -> int:
        """ Returns the Hamming weight of the given codeword """
        weight = 0
        for symbol in codeword:
            if symbol != '0':
                weight += 1
        return weight

    def get_min_weight(self) -> int:
        """ Returns the minimum Hamming weight of the codewords """
        if self._min_weight is None:
            self._min_weight = sys.maxsize
            for codeword in self.get_codewords():
                weight = LinearCode.get_weight(codeword)
                if weight < self._min_weight:
                    self._min_weight = weight

        return self._min_weight

    def _derive_codewords(self) -> None:
        raise NotImplementedError

    def get_codewords(self) -> Dict[str, List[str]]:
        """ Returns a dictionary of the (message -> codeword) mappings """
        if self._codewords is None:
            try:    # try-except block is just to change the message
                self._derive_codewords()
            except LinearCodeError:
                raise LinearCodeError("There are no codewords given and they can't be derived.")
        return self._codewords

    def set_codewords(self, codewords: Dict[str, List[str]]) -> None:
        """ Sets the codeword mappings """
        for elem in codewords.items():
            if len(elem[0]) != self.get_message_length() or len(elem[1]) != self.get_block_length():
                raise LinearCodeError("Message length or block length of " + str(elem) + " is not correct")

            for symbol in elem[1]:
                if symbol not in self.get_alphabet():
                    raise LinearCodeError("The symbol " + symbol + " should not be present in codeword " + str(elem[1]))

        self._codewords = codewords
        self._min_distance = None
        self._min_weight = None

    def _derive_generator_matrix(self) -> None:
        raise NotImplementedError

    def get_generator_matrix(self) -> List[List[str]]:
        """ Returns the generator matrix """
        if self._generator_matrix is None:
            try:
                self._derive_generator_matrix()
            except LinearCodeError:
                raise LinearCodeError("The generator matrix is not given and can't be derived")
        return self._generator_matrix

    def set_generator_matrix(self, generator_matrix: List[List[str]]) -> None:
        """ Sets the generator matrix """
        if len(generator_matrix) != self.get_message_length():
            raise LinearCodeError("The number of rows in the generator matrix must be equal to the length of"
                                  " original messages (k).")
        alphabet = self.get_alphabet()
        for row in generator_matrix:
            if len(row) != self.get_block_length():
                raise LinearCodeError("The number of columns in the generator matrix must be equal to the length of"
                                      " codewords")
            for symbol in row:
                if symbol not in alphabet:
                    raise LinearCodeError("The generator matrix can only contain symbols of the symbol alphabet")

        self._generator_matrix = generator_matrix

    def _derive_parity_check_matrix(self) -> None:
        raise NotImplementedError

    def get_parity_check_matrix(self) -> List[List[str]]:
        """ Returns the parity-check matrix """
        if self._parity_check_matrix is None:
            try:
                self._derive_parity_check_matrix()
            except LinearCodeError:
                raise LinearCodeError("The parity-check matrix is not given and can't be derived")
        return self._parity_check_matrix

    def set_parity_check_matrix(self, parity_check_matrix: List[List[str]]) -> None:
        """ Sets the parity-check matrix """
        if len(parity_check_matrix) != self.get_block_length() - self.get_message_length():
            raise LinearCodeError("The row number of the parity-check matrix should be "
                                  + str(self.get_block_length() - self.get_message_length()) + " (n-k).")
        for row in parity_check_matrix:
            if len(row) != self.get_block_length():
                raise LinearCodeError("The number of columns in a p-c. matrix must be equal to the length of codewords")
            alphabet = self.get_alphabet()
            for symbol in row:
                if symbol not in alphabet:
                    raise LinearCodeError("The p-c. matrix can only contain symbols of the symbol alphabet")

        self._parity_check_matrix = parity_check_matrix

    def _construct_standard_array(self) -> None:
        raise NotImplementedError

    def get_standard_array(self) -> List[List[str]]:
        """ Returns the standard (Slepian) array """
        if self._standard_array is None:
            try:
                self._construct_standard_array()
            except LinearCodeError:
                raise LinearCodeError("The Standard array is not given and can't be derived")
        return self._standard_array

    def set_standard_array(self, standard_array: List[List[str]]) -> None:
        """ Sets the standard (Slepian) array """
        nr_of_rows = 2 ** (self.get_block_length() - self.get_message_length())     # 2^(n-k) rows
        nr_of_columns = 1 + (2 ** self.get_message_length())
        if len(standard_array) != nr_of_rows:
            LinearCodeError("The row number of the standard array should be " + nr_of_rows)
        if len(standard_array[0]) != nr_of_columns:
            LinearCodeError("The column number of the standard array should be " + nr_of_columns)
        # more checks can be added later...

        self._standard_array = standard_array

    def decode(self, codeword: str) -> str:
        raise NotImplementedError


class LinearCodeError(Exception):
    """ A special error type for linear codes """
    pass


class BinaryCode(LinearCode):
    """ Represents a binary linear code """

    def __init__(self, message_length: int, block_length: int) -> None:
        """ Constructor """
        super().__init__(('0', '1'), message_length, block_length)

    def __get_codewords_from_standard_array(self):
        """ Gets the codewords from the Slepian array """
        standard_array = self._standard_array
        k = self.get_message_length()
        codewords = {}

        for i in range(1, len(standard_array[0])):
            codeword = standard_array[0][i]
            codewords[codeword[0:k]] = codeword

        self.set_codewords(codewords)

    def is_parity_check_matrix_in_standard_form(self) -> bool:
        """ Determines whether the parity-check matrix is in standard form """
        p_c_matrix = self.get_parity_check_matrix()
        identity_matrix = BinaryCode.create_identity_matrix(len(p_c_matrix))

        for i in range(len(p_c_matrix)):
            # Each row of the parity-check matrix should end in the same row of the identity matrix
            if not "".join(p_c_matrix[i]).endswith("".join(identity_matrix[i])):
                return False
        return True

    def _get_codewords_from_generator_matrix(self):
        """ Finds all the codewords by multiplying the generator matrix """
        # Generate all possible codewords
        nr_of_words = 2 ** self.get_message_length()  # binary code -> 2 ^ k possible codewords

        words = []
        for i in range(nr_of_words):
            words.append('')

        limit = nr_of_words / 2
        for col_idx in range(self.get_message_length()):
            one = True
            for row_idx in range(nr_of_words):
                if row_idx % limit == 0:        # if we reached the limit, change the number
                    one = not one
                words[row_idx] += '1' if one else '0'
            limit /= 2

        gen_matrix = self.get_generator_matrix()

        # Multiply the generator matrix with these, to get their codewords
        codewords = {}

        k = self.get_message_length()
        n = self.get_block_length()
        for word in words:
            codeword = ''
            for col_idx in range(n):
                one = False                 # True represents 1, False represents 0
                for row_idx in range(k):
                    if word[row_idx] == '1' and gen_matrix[row_idx][col_idx] == '1':
                        one = not one       # Adding 1 in binary: 0 -> 1, 1 -> 0
                codeword += '1' if one else '0'
            codewords[word] = codeword

        self.set_codewords(codewords)

    def _derive_codewords(self) -> None:
        """ Tries to derive the codewords """
        if self._standard_array is not None:
            self.__get_codewords_from_standard_array()
        elif self._generator_matrix is not None:
            self._get_codewords_from_generator_matrix()
        elif self._parity_check_matrix is not None:
            self._get_generator_matrix_from_parity_check_matrix()
            self._get_codewords_from_generator_matrix()
        else:
            raise LinearCodeError("The codewords can't be derived")     # Shouldn't happen

    def __derive_generator_matrix_from_codewords(self) -> None:
        """ Tries to derive the G from codewords """
        k = self.get_message_length()
        codewords = list(self.get_codewords().values())

        generator_matrix = codewords[0:k]  # initialization, G contains k rows
        found_identity_m_rows = 0
        for codeword in codewords:
            # try to find the rows of the standard form

            standard_position = None
            for i in range(k):
                # check the number of 1-s in the first k columns of the codeword
                if codeword[i] == '1':
                    if standard_position is None:
                        standard_position = i
                    else:
                        # if we found more than 1 non-zero element, the codeword won't be part of the standard form
                        standard_position = None
                        break

            if standard_position is not None:
                # set the found row of G
                generator_matrix[standard_position] = codeword
                found_identity_m_rows += 1
                if found_identity_m_rows == k:
                    break

        self.set_generator_matrix(generator_matrix)

    def __fill_generator_matrix(self, word: List[str]) -> None:
        """ Generates possible codewords and fill the generator matrix if they are codewords """
        if len(word) == self.get_block_length():
            # Don't put 00...0 in the generator matrix
            if not ('1' in word):
                return
            # If we got a word, check it's syndrome
            if not ('1' in self._get_syndrome(''.join(word))):
                # If the syndrome is 00...0, then word is a codeword
                self._generator_matrix.append(word[:])
            return

        word.append('0')
        self.__fill_generator_matrix(word)

        if len(self._generator_matrix) < self.get_message_length():
            del word[-1]
            word.append('1')
            self.__fill_generator_matrix(word)
            del word[-1]

    def _get_generator_matrix_from_parity_check_matrix(self):
        """ Gets the generator matrix from the parity-check matrix """
        if self.is_parity_check_matrix_in_standard_form():
            n = self.get_block_length()
            k = self.get_message_length()
            generator_matrix = self.create_identity_matrix(k)
            p_c_matrix = self.get_parity_check_matrix()
            for p_c_row_idx in range(n - k):
                for p_c_col_idx in range(k):
                    generator_matrix[p_c_col_idx].append(p_c_matrix[p_c_row_idx][p_c_col_idx])
            self.set_generator_matrix(generator_matrix)
            return

        # If it's not in standard form, generate k random codewords to get a generator matrix
        self._generator_matrix = []
        self.__fill_generator_matrix([])

    def _derive_generator_matrix(self) -> None:
        """ Tries to derive the generator matrix from codewords, parity-check matrix or the standard array """
        if self._codewords is not None:
            self.__derive_generator_matrix_from_codewords()
        elif self._parity_check_matrix is not None:
            self._get_generator_matrix_from_parity_check_matrix()
        elif self._standard_array is not None:
            self.__get_codewords_from_standard_array()
            self.__derive_generator_matrix_from_codewords()
        else:
            raise LinearCodeError("The generator matrix can't be derived")

    @staticmethod
    def create_identity_matrix(n: int) -> List[List[str]]:
        """ Creates an (n x n) identity matrix """
        matrix = []
        for i in range(n):
            row = ['0'] * n
            row[i] = '1'
            matrix.append(row)
        return matrix

    def is_gen_matrix_in_standard_form(self) -> bool:
        """ Determines whether the generator matrix is in standard form """
        generator_matrix = self.get_generator_matrix()
        identity_matrix = BinaryCode.create_identity_matrix(self.get_message_length())

        for i in range(len(generator_matrix)):
            # Each row of the generator matrix should start with the same row of the identity matrix
            if not "".join(generator_matrix[i]).startswith("".join(identity_matrix[i])):
                return False
        return True

    def _derive_parity_check_matrix(self) -> None:
        """ Tries to derive the parity-check matrix from the generator matrix or the standard array """
        if self._generator_matrix is None:
            self._derive_generator_matrix()

        if not self.is_gen_matrix_in_standard_form():
            # Get the standard form of G from codewords instead of row sums and column permutations
            if self._codewords is None:
                self._derive_codewords()
            self.__derive_generator_matrix_from_codewords()

        generator_matrix = self.get_generator_matrix()

        # Get H (or H' if generator_matrix is G')
        # Fill the first k columns
        n_k = self.get_block_length() - self.get_message_length()
        k = len(generator_matrix)
        p_c_matrix = [[] for i in range(n_k)]
        for i in range(k):
            column = generator_matrix[i][k:]
            for j in range(n_k):
                p_c_matrix[j].append(column[j])
        # Append the identity matrix
        identity_matrix = BinaryCode.create_identity_matrix(n_k)
        for i in range(len(p_c_matrix)):
            for elem in identity_matrix[i]:
                p_c_matrix[i].append(elem)

        self._parity_check_matrix = p_c_matrix

    @staticmethod
    def sum(codeword1: str, codeword2: str) -> str:
        """ returns the "sum" of two binary codewords (not real sum) """
        if len(codeword1) != len(codeword2):
            raise LinearCodeError("Please give equal length codewords for calculating their sum")

        result = ''
        for i in range(len(codeword1)):
            result += '0' if codeword1[i] == codeword2[i] else '1'
        return result

    def _get_syndrome(self, code: str) -> str:
        n = len(code)
        if n != self.get_block_length():
            raise LinearCodeError("Can't get the syndrome, the code's length is not appropriate.")

        syndrome = ''

        # Multiply the parity-check matrix's rows with the codeword's transpose to get the syndrome
        for row in self.get_parity_check_matrix():
            one = False                 # True represents 1, False represents 0
            for i in range(n):
                if row[i] == '1' and code[i] == '1':
                    one = not one       # Adding 1 in binary: 0 -> 1, 1 -> 0
            syndrome += '1' if one else '0'

        return syndrome

    def _construct_standard_array(self) -> None:
        """ Tries to derive the Slepian array using the parity-check matrix and the codewords """
        p_c_matrix = self.get_parity_check_matrix()

        # Get the codewords as strings and sort them alphabetically
        codewords = []
        for codeword in self.get_codewords().values():
            codewords.append(''.join(codeword))
        codewords.sort()

        # Create the n-k rows of the standard array and leave the syndrome's column empty for now
        standard_array = []
        nr_of_rows = 2 ** (self.get_block_length() - self.get_message_length())     # 2^(n-k)
        for i in range(nr_of_rows):
            standard_array.append([''])

        # Generate weight=1 codes in alphabetic order
        n = self.get_block_length()
        weight_1 = []
        for i in range(n):
            code = '0' * (n - i - 1)
            code += '1'
            code += '0' * i
            weight_1.append(code)

        # Fill the first row of the Slepian array
        for codeword in codewords:
            standard_array[0].append(codeword)
            # if the codeword appears in the weight_1 list, remove it
            if codeword in weight_1:
                weight_1.remove(codeword)

        nr_of_columns = len(standard_array[0])

        # Fill the Slepian array's rows having cosets weighing 1
        current_row = 1
        while len(weight_1) > 0:
            # add coset
            coset = weight_1[0]
            standard_array[current_row].append(coset)
            weight_1.remove(coset)

            for i in range(2, nr_of_columns):
                code = self.sum(standard_array[0][i], coset)
                standard_array[current_row].append(code)
                if code in weight_1:
                    weight_1.remove(code)
            current_row += 1

        # Fill the rest of the Slepian array
        # Generate cosets weighing 2
        first_1 = n - 2             # The codeword has two 1-s, this is the first 1's index
        second_1 = n - 1            # This is the second 1's index
        while current_row < nr_of_rows:
            # Find the next coset
            coset = None
            while coset is None:
                code = '0' * first_1
                code += '1'
                code += '0' * (second_1 - first_1 - 1)
                code += '1'
                code += '0' * (n - second_1 - 1)

                # Check whether the generated code is already in the previous rows of the array
                code_found = False
                for i in range(current_row):
                    if code in standard_array[i]:
                        code_found = True
                        break

                # If the code didn't appear, it will be the next coset
                if not code_found:
                    coset = code

                # Move the indexes of 1's to find the next code
                if second_1 - 1 == first_1:
                    # We need to move left with the first 1
                    first_1 -= 1
                    # And go start the second_1 at the end. E.g: 000110 -> 001001
                    second_1 = n - 1

                    if first_1 < 0:
                        raise LinearCodeError("Sorry, the current algorithm doesn't support generating cosets weighing "
                                              "more than 2. That requires a different approach.")
                else:
                    second_1 -= 1

            # Fill the current row
            standard_array[current_row].append(coset)
            for i in range(2, nr_of_columns):
                standard_array[current_row].append(self.sum(standard_array[0][i], coset))

            current_row += 1

        # Set the syndromes of each row of the array
        for i in range(len(standard_array)):
            standard_array[i][0] = self._get_syndrome(standard_array[i][1])

        self._standard_array = standard_array

    def decode(self, codeword: str) -> str:
        """ Decodes the given faulty word """
        codeword_syndrome = self._get_syndrome(codeword)
        standard_array = self.get_standard_array()

        coset = None
        for i in range(len(standard_array)):
            if standard_array[i][0] == codeword_syndrome:
                coset = standard_array[i][1]

        if coset is None:
            raise LinearCodeError("Couldn't find the codeword's syndrome in the Slepian array.")

        return self.sum(codeword, coset)
