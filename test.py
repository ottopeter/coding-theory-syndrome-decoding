from error_control.error_control_io import ECIO

test_C_4_2_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/C_4_2_ex1.txt')
test_C_5_2_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/C_5_2_ex1.txt')
test_C_5_2_ex2 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/C_5_2_ex2.txt')
test_C_6_3_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/C_6_3_ex1.txt')
test_G_6_3_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/G_6_3_ex1.txt')
test_GC_6_3_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/GC_6_3_ex1.txt')
test_H_4_1_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/H_4_1_ex1.txt')
test_H_5_2_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/H_5_2_ex1.txt')
test_S_6_3_ex1 = ECIO.read_lin_code_from_file('inputs/binary_codes/examples/S_6_3_ex1.txt')

# ECIO.write_lin_code(test_C_4_2_ex1)
# ECIO.write_lin_code(test_C_5_2_ex1)
# ECIO.write_lin_code(test_C_5_2_ex2)
# ECIO.write_lin_code(test_C_6_3_ex1)
# ECIO.write_lin_code(test_G_6_3_ex1)
# ECIO.write_lin_code(test_GC_6_3_ex1)
# ECIO.write_lin_code(test_H_4_1_ex1)
ECIO.write_lin_code(test_H_5_2_ex1)
# ECIO.write_lin_code(test_S_6_3_ex1)

print(test_H_5_2_ex1.decode('11111'))
print(test_H_5_2_ex1.decode('11001'))
